/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EContractService } from './EContract.service';
import 'rxjs/add/operator/toPromise';
import { UserSelectService } from '../user-select/user-select.service';

@Component({
  selector: 'app-econtract',
  templateUrl: './EContract.component.html',
  styleUrls: ['./EContract.component.css'],
  providers: [EContractService]
})
export class EContractComponent implements OnInit {

  myForm: FormGroup;

  private allAssets;
  private asset;
  private currentId;
  private errorMessage;
  private searchValue;
  private currentUser;

  caseid = new FormControl('', Validators.required);
  property = new FormControl('', Validators.required);
  amount = new FormControl('', Validators.required);
  contractState = new FormControl('', Validators.required);
  pSolicitorName = new FormControl('', Validators.required);
  pSolicitorFirm = new FormControl('', Validators.required);
  vSolicitorName = new FormControl('', Validators.required);
  vSolicitorFirm = new FormControl('', Validators.required);
  vendorName = new FormControl('', Validators.required);
  vendorAddress = new FormControl('', Validators.required);
  vendorIdno = new FormControl('', Validators.required);
  purchaserName = new FormControl('', Validators.required);
  purchaserAddress = new FormControl('', Validators.required);
  purchaserIdno = new FormControl('', Validators.required);
  terms = new FormControl('', Validators.required);
  purchaserEndorse = new FormControl('', Validators.required);
  vendorEndorse = new FormControl('', Validators.required);
  pSolicitorEndorse = new FormControl('', Validators.required);
  vSolicitorEndorse = new FormControl('', Validators.required);
  endorse = new FormControl('', Validators.required);
  active = new FormControl('', Validators.required);
  owner = new FormControl('', Validators.required);

  // constructor(public serviceEContract: EContractService, fb: FormBuilder, public UserSelect: UserSelectService) {
  constructor(public serviceEContract: EContractService, fb: FormBuilder) {
    this.myForm = fb.group({
      caseid: this.caseid,
      property: this.property,
      amount: this.amount,
      contractState: this.contractState,
      pSolicitorName: this.pSolicitorName,
      pSolicitorFirm: this.pSolicitorFirm,
      vSolicitorName: this.vSolicitorName,
      vSolicitorFirm: this.vSolicitorFirm,
      vendorName: this.vendorName,
      vendorAddress: this.vendorAddress,
      vendorIdno: this.vendorIdno,
      purchaserName: this.purchaserName,
      purchaserAddress: this.purchaserAddress,
      purchaserIdno: this.purchaserIdno,
      terms: this.terms,
      purchaserEndorse: this.purchaserEndorse,
      vendorEndorse: this.vendorEndorse,
      pSolicitorEndorse: this.pSolicitorEndorse,
      vSolicitorEndorse: this.vSolicitorEndorse,
      endorse: this.endorse,
      active: this.active,
      owner: this.owner
    });
    //console.log(UserSelect.getCurrentUser());
  };

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceEContract.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(asset => {
        tempList.push(asset);
      });
      this.allAssets = tempList;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the asset field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the asset updateDialog.
   * @param {String} name - the name of the asset field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified asset field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  addAsset(form: any): Promise<any> {
    this.asset = {
      $class: 'org.example.namespacetest.EContract',
      'caseid': this.caseid.value,
      'property': this.property.value,
      'amount': this.amount.value,
      'contractState': this.contractState.value,
      'pSolicitorName': this.pSolicitorName.value,
      'pSolicitorFirm': this.pSolicitorFirm.value,
      'vSolicitorName': this.vSolicitorName.value,
      'vSolicitorFirm': this.vSolicitorFirm.value,
      'vendorName': this.vendorName.value,
      'vendorAddress': this.vendorAddress.value,
      'vendorIdno': this.vendorIdno.value,
      'purchaserName': this.purchaserName.value,
      'purchaserAddress': this.purchaserAddress.value,
      'purchaserIdno': this.purchaserIdno.value,
      'terms': this.terms.value,
      'purchaserEndorse': this.purchaserEndorse.value,
      'vendorEndorse': this.vendorEndorse.value,
      'pSolicitorEndorse': this.pSolicitorEndorse.value,
      'vSolicitorEndorse': this.vSolicitorEndorse.value,
      'endorse': this.endorse.value,
      'active': this.active.value,
      'owner': this.owner.value
    };

    this.myForm.setValue({
      'caseid': null,
      'property': null,
      'amount': null,
      'contractState': null,
      'pSolicitorName': null,
      'pSolicitorFirm': null,
      'vSolicitorName': null,
      'vSolicitorFirm': null,
      'vendorName': null,
      'vendorAddress': null,
      'vendorIdno': null,
      'purchaserName': null,
      'purchaserAddress': null,
      'purchaserIdno': null,
      'terms': null,
      'purchaserEndorse': null,
      'vendorEndorse': null,
      'pSolicitorEndorse': null,
      'vSolicitorEndorse': null,
      'endorse': null,
      'active': null,
      'owner': null
    });

    return this.serviceEContract.addAsset(this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.myForm.setValue({
        'caseid': null,
        'property': null,
        'amount': null,
        'contractState': null,
        'pSolicitorName': null,
        'pSolicitorFirm': null,
        'vSolicitorName': null,
        'vSolicitorFirm': null,
        'vendorName': null,
        'vendorAddress': null,
        'vendorIdno': null,
        'purchaserName': null,
        'purchaserAddress': null,
        'purchaserIdno': null,
        'terms': null,
        'purchaserEndorse': null,
        'vendorEndorse': null,
        'pSolicitorEndorse': null,
        'vSolicitorEndorse': null,
        'endorse': null,
        'active': true,
        'owner': null
      });
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
          this.errorMessage = error;
      }
    });
  }


  updateAsset(form: any): Promise<any> {
    console.log("purchaserEndorse:"+ this.purchaserEndorse.value);
    console.log("vendorEndorse:"+ this.vendorEndorse.value);
    console.log("pSolicitorEndorse:"+ this.pSolicitorEndorse.value);
    console.log("vSolicitorEndorse:"+ this.vSolicitorEndorse.value);
    console.log("active:"+ this.active.value);
    console.log("endorse count:" + this.endorse.value);
    var pEndorse = false;
    var vEndorse = false;
    var pSEndorse = false;
    var vSEndorse = false;
    var active = false;
    var endorse_count = 0;
    if(this.active.value){
      active = this.active.value;
    }
    if(this.endorse.value){
      endorse_count = this.endorse.value;
    }
    if(this.purchaserEndorse.value){
      pEndorse = this.purchaserEndorse.value;
    }
    if(this.vendorEndorse.value){
      vEndorse = this.vendorEndorse.value;
    }
    if(this.pSolicitorEndorse.value){
      pSEndorse = this.pSolicitorEndorse.value;
    }
    if(this.vSolicitorEndorse.value){
      vSEndorse = this.vSolicitorEndorse.value;
    }


    
    this.asset = {
      $class: 'org.example.namespacetest.EContract',
      'caseid': this.caseid.value,
      'property': this.property.value,
      'amount': this.amount.value,
      'contractState': this.contractState.value,
      'pSolicitorName': this.pSolicitorName.value,
      'pSolicitorFirm': this.pSolicitorFirm.value,
      'vSolicitorName': this.vSolicitorName.value,
      'vSolicitorFirm': this.vSolicitorFirm.value,
      'vendorName': this.vendorName.value,
      'vendorAddress': this.vendorAddress.value,
      'vendorIdno': this.vendorIdno.value,
      'purchaserName': this.purchaserName.value,
      'purchaserAddress': this.purchaserAddress.value,
      'purchaserIdno': this.purchaserIdno.value,
      'terms': this.terms.value,
      'purchaserEndorse': pEndorse,
      'vendorEndorse': vEndorse,
      'pSolicitorEndorse': pSEndorse,
      'vSolicitorEndorse': vSEndorse,
      'endorse': endorse_count,
      'active': active,
      'owner': this.owner.value
    };

    return this.serviceEContract.updateAsset(form.get('caseid').value, this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }


  deleteAsset(): Promise<any> {

    return this.serviceEContract.deleteAsset(this.currentId)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  getForm(id: any): Promise<any> {

    return this.serviceEContract.getAsset(id)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      const formObject = {
        'caseid': null,
        'property': null,
        'amount': null,
        'contractState': null,
        'pSolicitorName': null,
        'pSolicitorFirm': null,
        'vSolicitorName': null,
        'vSolicitorFirm': null,
        'vendorName': null,
        'vendorAddress': null,
        'vendorIdno': null,
        'purchaserName': null,
        'purchaserAddress': null,
        'purchaserIdno': null,
        'terms': null,
        'purchaserEndorse': null,
        'vendorEndorse': null,
        'pSolicitorEndorse': null,
        'vSolicitorEndorse': null,
        'endorse': null,
        'active': null,
        'owner': null
      };

      if (result.caseid) {
        formObject.caseid = result.caseid;
      } else {
        formObject.caseid = null;
      }

      if (result.property) {
        formObject.property = result.property;
      } else {
        formObject.property = null;
      }

      if (result.amount) {
        formObject.amount = result.amount;
      } else {
        formObject.amount = null;
      }

      if (result.contractState) {
        formObject.contractState = result.contractState;
      } else {
        formObject.contractState = null;
      }

      if (result.pSolicitorName) {
        formObject.pSolicitorName = result.pSolicitorName;
      } else {
        formObject.pSolicitorName = null;
      }

      if (result.pSolicitorFirm) {
        formObject.pSolicitorFirm = result.pSolicitorFirm;
      } else {
        formObject.pSolicitorFirm = null;
      }

      if (result.vSolicitorName) {
        formObject.vSolicitorName = result.vSolicitorName;
      } else {
        formObject.vSolicitorName = null;
      }

      if (result.vSolicitorFirm) {
        formObject.vSolicitorFirm = result.vSolicitorFirm;
      } else {
        formObject.vSolicitorFirm = null;
      }

      if (result.vendorName) {
        formObject.vendorName = result.vendorName;
      } else {
        formObject.vendorName = null;
      }

      if (result.vendorAddress) {
        formObject.vendorAddress = result.vendorAddress;
      } else {
        formObject.vendorAddress = null;
      }

      if (result.vendorIdno) {
        formObject.vendorIdno = result.vendorIdno;
      } else {
        formObject.vendorIdno = null;
      }

      if (result.purchaserName) {
        formObject.purchaserName = result.purchaserName;
      } else {
        formObject.purchaserName = null;
      }

      if (result.purchaserAddress) {
        formObject.purchaserAddress = result.purchaserAddress;
      } else {
        formObject.purchaserAddress = null;
      }

      if (result.purchaserIdno) {
        formObject.purchaserIdno = result.purchaserIdno;
      } else {
        formObject.purchaserIdno = null;
      }

      if (result.terms) {
        formObject.terms = result.terms;
      } else {
        formObject.terms = null;
      }

      if (result.purchaserEndorse) {
        formObject.purchaserEndorse = result.purchaserEndorse;
      } else {
        formObject.purchaserEndorse = null;
      }

      if (result.vendorEndorse) {
        formObject.vendorEndorse = result.vendorEndorse;
      } else {
        formObject.vendorEndorse = null;
      }

      if (result.pSolicitorEndorse) {
        formObject.pSolicitorEndorse = result.pSolicitorEndorse;
      } else {
        formObject.pSolicitorEndorse = null;
      }

      if (result.vSolicitorEndorse) {
        formObject.vSolicitorEndorse = result.vSolicitorEndorse;
      } else {
        formObject.vSolicitorEndorse = null;
      }

      if (result.endorse) {
        formObject.endorse = result.endorse;
      } else {
        formObject.endorse = null;
      }

      if (result.active) {
        formObject.active = result.active;
      } else {
        formObject.active = null;
      }

      if (result.owner) {
        formObject.owner = result.owner;
      } else {
        formObject.owner = null;
      }

      this.myForm.setValue(formObject);

    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  resetForm(): void {
    this.myForm.setValue({
      'caseid': null,
      'property': null,
      'amount': null,
      'contractState': 'Draft',
      'pSolicitorName': null,
      'pSolicitorFirm': null,
      'vSolicitorName': null,
      'vSolicitorFirm': null,
      'vendorName': null,
      'vendorAddress': null,
      'vendorIdno': null,
      'purchaserName': null,
      'purchaserAddress': null,
      'purchaserIdno': null,
      'terms': null,
      'purchaserEndorse': false,
      'vendorEndorse': false,
      'pSolicitorEndorse': false,
      'vSolicitorEndorse': false,
      'endorse': null,
      'active': true,
      'owner': null
      });
  }
  filterSearch(criteria: string, searchText: string): Promise<any> {
    const tempList = [];
    return this.serviceEContract.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(asset => {
        console.log("criteria: " + criteria)
        console.log("searchText: " + searchText);
        //if(searchText != ''){
          if(criteria == "caseid"){
            if(asset.caseid.includes(searchText)){
              tempList.push(asset);
            }
          }else if(criteria == "status"){
            if(asset.contractState.toString().includes(searchText)){
              tempList.push(asset);
            }
          }else if(criteria == "purchaser"){
            if(asset.pSolicitorFirm.includes(searchText) || asset.pSolicitorName.includes(searchText)) {
             tempList.push(asset);
            }
          }else if(criteria == "vendor"){
            if(asset.vSolicitorFirm.includes(searchText) || asset.vSolicitorName.includes(searchText)){
             tempList.push(asset);
            }
          }else if(criteria == "active"){
            if(asset.active.toString().includes(searchText)){
             tempList.push(asset);
            }
          }
        //else if (asset.owner.username.includes("testuser2")){
        //   tempList.push(asset);
        //}
      });
      this.allAssets = tempList;
      if (this.allAssets.length == 0){
        this.errorMessage = 'No Results Found with current search text';
      }
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }
  transform(items: any[], searchText: string): any[] {

    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.toLocaleLowerCase().includes(searchText);
    });
  }

}
