/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EndorseEContractService } from './EndorseEContract.service';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-endorseecontract',
  templateUrl: './EndorseEContract.component.html',
  styleUrls: ['./EndorseEContract.component.css'],
  providers: [EndorseEContractService]
})
export class EndorseEContractComponent implements OnInit {

  myForm: FormGroup;
  dataForm: FormGroup;

  private allTransactions;
  private Transaction;
  private currentId;
  private errorMessage;
  private currentEContractID;
  private currentUser;
  private successMessage;

  uType = new FormControl('', Validators.required);
  user = new FormControl('', Validators.required);
  contract = new FormControl('', Validators.required);
  transactionId = new FormControl('', Validators.required);
  timestamp = new FormControl('', Validators.required);


  caseid = new FormControl('', Validators.required);
  property = new FormControl('', Validators.required);
  amount = new FormControl('', Validators.required);
  contractState = new FormControl('', Validators.required);
  pSolicitorName = new FormControl('', Validators.required);
  pSolicitorFirm = new FormControl('', Validators.required);
  vSolicitorName = new FormControl('', Validators.required);
  vSolicitorFirm = new FormControl('', Validators.required);
  vendorName = new FormControl('', Validators.required);
  vendorAddress = new FormControl('', Validators.required);
  vendorIdno = new FormControl('', Validators.required);
  purchaserName = new FormControl('', Validators.required);
  purchaserAddress = new FormControl('', Validators.required);
  purchaserIdno = new FormControl('', Validators.required);
  terms = new FormControl('', Validators.required);
  purchaserEndorse = new FormControl('', Validators.required);
  vendorEndorse = new FormControl('', Validators.required);
  pSolicitorEndorse = new FormControl('', Validators.required);
  vSolicitorEndorse = new FormControl('', Validators.required);
  endorse = new FormControl('', Validators.required);
  active = new FormControl('', Validators.required);
  owner = new FormControl('', Validators.required);



  constructor(private serviceEndorseEContract: EndorseEContractService, fb: FormBuilder, private route: ActivatedRoute) {
    
    this.currentId = route.snapshot.paramMap.get("id");
    console.log(this.currentId);
    this.myForm = fb.group({
      uType: this.uType,
      user: this.user,
      // contract: this.currentId,
      contract: this.currentId,
      transactionId: this.transactionId,
      timestamp: this.timestamp
    });
    this.dataForm = fb.group({
      caseid: this.caseid,
      property: this.property,
      amount: this.amount,
      contractState: this.contractState,
      pSolicitorName: this.pSolicitorName,
      pSolicitorFirm: this.pSolicitorFirm,
      vSolicitorName: this.vSolicitorName,
      vSolicitorFirm: this.vSolicitorFirm,
      vendorName: this.vendorName,
      vendorAddress: this.vendorAddress,
      vendorIdno: this.vendorIdno,
      purchaserName: this.purchaserName,
      purchaserAddress: this.purchaserAddress,
      purchaserIdno: this.purchaserIdno,
      terms: this.terms,
      purchaserEndorse: this.purchaserEndorse,
      vendorEndorse: this.vendorEndorse,
      pSolicitorEndorse: this.pSolicitorEndorse,
      vSolicitorEndorse: this.vSolicitorEndorse,
      endorse: this.endorse,
      active: this.active,
      owner: this.owner
    });
    
    
  };

  ngOnInit(): void {
    //console.log(route.snapshot.paramMap.get("id"));
    //this.loadAll();
    this.getForm(this.currentId);


    //this.route.paramMap.subscribe(params => {
      //console.log(params.toString());
      // this.allTransactions.forEach((t: any) => {
      //   console.log(params.toString());
      //   if (t.id == params.keys){
      //     // .id) {
      //     this.Transaction = t;
      //   }
      //  });
    //});
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceEndorseEContract.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(transaction => {
        tempList.push(transaction);
      });
      this.allTransactions = tempList;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the transaction field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the transaction updateDialog.
   * @param {String} name - the name of the transaction field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified transaction field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  addTransaction(form: any): Promise<any> {
    this.errorMessage = null;
    this.successMessage = null;
    this.Transaction = {
      $class: 'org.example.namespacetest.EndorseEContract',
      'uType': this.uType.value,
      'user': this.user.value,
      'contract': this.currentId
      // ,
      // // 'contract': this.contract.value,
      // 'transactionId': this.transactionId.value,
      // 'timestamp': this.timestamp.value
    };

    // this.myForm.setValue({
    //   'uType': null,
    //   'user': null,
    //   'contract': null,
    //   'transactionId': null,
    //   'timestamp': null
    // });
    if(this.contractState.value != "Close"){
      if(this.uType.value === ""){
        this.errorMessage = "Please Select User Type of endorser";
      }else if(this.user.value === ""){
        this.errorMessage = "Plesae input userID of endorser";
      }else{
        return this.serviceEndorseEContract.addTransaction(this.Transaction)
        .toPromise()
        .then(() => {
          this.errorMessage = null;
          this.myForm.setValue({
            'uType': null,
            'user': null,
            'contract': null,
            'transactionId': null,
            'timestamp': null
          });
          this.successMessage = "EContract was successfully Endorsed";
        })
        .catch((error) => {
          if (error === 'Server error') {
            this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
          } else {
            this.errorMessage = error;
          }
        });
       }
    }else{
      this.errorMessage = "Contract is finialized and no longer available for endorsing.";
    }
  }

  updateTransaction(form: any): Promise<any> {
    this.Transaction = {
      $class: 'org.example.namespacetest.EndorseEContract',
      'uType': this.uType.value,
      'user': this.user.value,
      'contract': this.contract.value,
      'timestamp': this.timestamp.value
    };

    return this.serviceEndorseEContract.updateTransaction(form.get('transactionId').value, this.Transaction)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
      this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  deleteTransaction(): Promise<any> {

    return this.serviceEndorseEContract.deleteTransaction(this.currentId)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  // getForm(id: any): Promise<any> {

  //   return this.serviceEndorseEContract.getTransaction(id)
  //   .toPromise()
  //   .then((result) => {
  //     this.errorMessage = null;
  //     const formObject = {
  //       'uType': null,
  //       'user': null,
  //       'contract': null,
  //       'transactionId': null,
  //       'timestamp': null
  //     };

  //     if (result.uType) {
  //       formObject.uType = result.uType;
  //     } else {
  //       formObject.uType = null;
  //     }

  //     if (result.user) {
  //       formObject.user = result.user;
  //     } else {
  //       formObject.user = null;
  //     }

  //     if (result.contract) {
  //       formObject.contract = result.contract;
  //     } else {
  //       formObject.contract = null;
  //     }

  //     if (result.transactionId) {
  //       formObject.transactionId = result.transactionId;
  //     } else {
  //       formObject.transactionId = null;
  //     }

  //     if (result.timestamp) {
  //       formObject.timestamp = result.timestamp;
  //     } else {
  //       formObject.timestamp = null;
  //     }

  //     this.myForm.setValue(formObject);

  //   })
  //   .catch((error) => {
  //     if (error === 'Server error') {
  //       this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
  //     } else if (error === '404 - Not Found') {
  //     this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
  //     } else {
  //       this.errorMessage = error;
  //     }
  //   });
  // }

    getForm(id: any): Promise<any> {

    return this.serviceEndorseEContract.getAsset(id)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      const formObject = {
        'caseid': null,
        'property': null,
        'amount': null,
        'contractState': null,
        'pSolicitorName': null,
        'pSolicitorFirm': null,
        'vSolicitorName': null,
        'vSolicitorFirm': null,
        'vendorName': null,
        'vendorAddress': null,
        'vendorIdno': null,
        'purchaserName': null,
        'purchaserAddress': null,
        'purchaserIdno': null,
        'terms': null,
        'purchaserEndorse': null,
        'vendorEndorse': null,
        'pSolicitorEndorse': null,
        'vSolicitorEndorse': null,
        'endorse': null,
        'active': null,
        'owner': null
      };

      if (result.caseid) {
        formObject.caseid = result.caseid;
      } else {
        formObject.caseid = null;
      }

      if (result.property) {
        formObject.property = result.property;
      } else {
        formObject.property = null;
      }

      if (result.amount) {
        formObject.amount = result.amount;
      } else {
        formObject.amount = null;
      }

      if (result.contractState) {
        formObject.contractState = result.contractState;
      } else {
        formObject.contractState = null;
      }

      if (result.pSolicitorName) {
        formObject.pSolicitorName = result.pSolicitorName;
      } else {
        formObject.pSolicitorName = null;
      }

      if (result.pSolicitorFirm) {
        formObject.pSolicitorFirm = result.pSolicitorFirm;
      } else {
        formObject.pSolicitorFirm = null;
      }

      if (result.vSolicitorName) {
        formObject.vSolicitorName = result.vSolicitorName;
      } else {
        formObject.vSolicitorName = null;
      }

      if (result.vSolicitorFirm) {
        formObject.vSolicitorFirm = result.vSolicitorFirm;
      } else {
        formObject.vSolicitorFirm = null;
      }

      if (result.vendorName) {
        formObject.vendorName = result.vendorName;
      } else {
        formObject.vendorName = null;
      }

      if (result.vendorAddress) {
        formObject.vendorAddress = result.vendorAddress;
      } else {
        formObject.vendorAddress = null;
      }

      if (result.vendorIdno) {
        formObject.vendorIdno = result.vendorIdno;
      } else {
        formObject.vendorIdno = null;
      }

      if (result.purchaserName) {
        formObject.purchaserName = result.purchaserName;
      } else {
        formObject.purchaserName = null;
      }

      if (result.purchaserAddress) {
        formObject.purchaserAddress = result.purchaserAddress;
      } else {
        formObject.purchaserAddress = null;
      }

      if (result.purchaserIdno) {
        formObject.purchaserIdno = result.purchaserIdno;
      } else {
        formObject.purchaserIdno = null;
      }

      if (result.terms) {
        formObject.terms = result.terms;
      } else {
        formObject.terms = null;
      }

      if (result.purchaserEndorse) {
        formObject.purchaserEndorse = result.purchaserEndorse;
      } else {
        formObject.purchaserEndorse = null;
      }

      if (result.vendorEndorse) {
        formObject.vendorEndorse = result.vendorEndorse;
      } else {
        formObject.vendorEndorse = null;
      }

      if (result.pSolicitorEndorse) {
        formObject.pSolicitorEndorse = result.pSolicitorEndorse;
      } else {
        formObject.pSolicitorEndorse = null;
      }

      if (result.vSolicitorEndorse) {
        formObject.vSolicitorEndorse = result.vSolicitorEndorse;
      } else {
        formObject.vSolicitorEndorse = null;
      }

      if (result.endorse) {
        formObject.endorse = result.endorse;
      } else {
        formObject.endorse = null;
      }

      if (result.active) {
        formObject.active = result.active;
      } else {
        formObject.active = null;
      }

      if (result.owner) {
        formObject.owner = result.owner;
      } else {
        formObject.owner = null;
      }

      this.dataForm.setValue(formObject);

    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  resetForm(): void {
    this.myForm.setValue({
      'uType': null,
      'user': null,
      'contract': null,
      'transactionId': null,
      'timestamp': null
    });
  }
}
