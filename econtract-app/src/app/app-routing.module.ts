/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

import { EContractComponent } from './EContract/EContract.component';

import { UserComponent } from './User/User.component';

import { ProcessEContractComponent } from './ProcessEContract/ProcessEContract.component';
import { EndorseEContractComponent } from './EndorseEContract/EndorseEContract.component';
import { UpdateEContractComponent } from './UpdateEContract/UpdateEContract.component';
import { ReviewEContractComponent } from './ReviewEContract/ReviewEContract.component';
import { CreateNewContractComponent } from './create-new-contract/create-new-contract.component';
import { UploadDataComponent } from './upload-data/upload-data.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'EContract', component: EContractComponent },
  { path: 'User', component: UserComponent },
  { path: 'ProcessEContract/:id', component: ProcessEContractComponent },
  //{ path: 'EndorseEContract', component: EndorseEContractComponent },
  { path: 'EndorseEContract/:id', component: EndorseEContractComponent },
  { path: 'UpdateEContract/:id', component: UpdateEContractComponent },
  { path: 'ReviewEContract/:id', component: ReviewEContractComponent },
  { path: 'CreateNewContract', component: CreateNewContractComponent },
  { path: 'uploadData', component: UploadDataComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
 imports: [RouterModule.forRoot(routes)],
 exports: [RouterModule],
 providers: []
})
export class AppRoutingModule { }
