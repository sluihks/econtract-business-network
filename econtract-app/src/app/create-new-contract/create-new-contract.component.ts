import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CreateNewContractService } from './create-new-contract.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-create-new-contract',
  templateUrl: './create-new-contract.component.html',
  styleUrls: ['./create-new-contract.component.css'],
  providers: [CreateNewContractService]
})
export class CreateNewContractComponent implements OnInit {

  myForm: FormGroup;

  private asset;
  private errorMessage;
  private successMessage;

  caseid = new FormControl('', Validators.required);
  property = new FormControl('', Validators.required);
  amount = new FormControl('', Validators.required);
  contractState = new FormControl('Draft', Validators.required);
  pSolicitorName = new FormControl('', Validators.required);
  pSolicitorFirm = new FormControl('', Validators.required);
  vSolicitorName = new FormControl('', Validators.required);
  vSolicitorFirm = new FormControl('', Validators.required);
  vendorName = new FormControl('', Validators.required);
  vendorAddress = new FormControl('', Validators.required);
  vendorIdno = new FormControl('', Validators.required);
  purchaserName = new FormControl('', Validators.required);
  purchaserAddress = new FormControl('', Validators.required);
  purchaserIdno = new FormControl('', Validators.required);
  terms = new FormControl('', Validators.required);
  purchaserEndorse = new FormControl(false, Validators.required);
  vendorEndorse = new FormControl(false, Validators.required);
  pSolicitorEndorse = new FormControl(false, Validators.required);
  vSolicitorEndorse = new FormControl(false, Validators.required);
  endorse = new FormControl(0, Validators.required);
  active = new FormControl(true, Validators.required);
  owner = new FormControl('', Validators.required);




  constructor(public serviceNewContract: CreateNewContractService, fb: FormBuilder) {
    this.myForm = fb.group({
      caseid: this.caseid,
      property: this.property,
      amount: this.amount,
      contractState: this.contractState,
      pSolicitorName: this.pSolicitorName,
      pSolicitorFirm: this.pSolicitorFirm,
      vSolicitorName: this.vSolicitorName,
      vSolicitorFirm: this.vSolicitorFirm,
      vendorName: this.vendorName,
      vendorAddress: this.vendorAddress,
      vendorIdno: this.vendorIdno,
      purchaserName: this.purchaserName,
      purchaserAddress: this.purchaserAddress,
      purchaserIdno: this.purchaserIdno,
      terms: this.terms,
      purchaserEndorse: this.purchaserEndorse,
      vendorEndorse: this.vendorEndorse,
      pSolicitorEndorse: this.pSolicitorEndorse,
      vSolicitorEndorse: this.vSolicitorEndorse,
      endorse: this.endorse,
      active: this.active,
      owner: this.owner
    });
  };

  ngOnInit() {

      this.errorMessage = null;
      this.successMessage = null;
  }
  
  /*
      Function call to add a new EContract to blockchain. 
      Returns Success if contract was added to blockchain
      Returns Error if it was rejected and not added to blockchain
  */
  addAsset(form: any): Promise<any> {
    this.successMessage = "";
    this.errorMessage = "";
    this.asset = {
      $class: 'org.example.namespacetest.EContract',
      'caseid': "ec" + this.caseid.value,
      'property': this.property.value,
      'amount': this.amount.value,
      'contractState': this.contractState.value,
      'pSolicitorName': this.pSolicitorName.value,
      'pSolicitorFirm': this.pSolicitorFirm.value,
      'vSolicitorName': this.vSolicitorName.value,
      'vSolicitorFirm': this.vSolicitorFirm.value,
      'vendorName': this.vendorName.value,
      'vendorAddress': this.vendorAddress.value,
      'vendorIdno': this.vendorIdno.value,
      'purchaserName': this.purchaserName.value,
      'purchaserAddress': this.purchaserAddress.value,
      'purchaserIdno': this.purchaserIdno.value,
      'terms': this.terms.value,
      'purchaserEndorse': this.purchaserEndorse.value,
      'vendorEndorse': this.vendorEndorse.value,
      'pSolicitorEndorse': this.pSolicitorEndorse.value,
      'vSolicitorEndorse': this.vSolicitorEndorse.value,
      'endorse': this.endorse.value,
      'active': this.active.value,
      'owner': this.owner.value
    };
    // this.myForm.setValue({
    //   'caseid': null,
    //   'property': null,
    //   'amount': null,
    //   'contractState': 'Draft',
    //   'pSolicitorName': null,
    //   'pSolicitorFirm': null,
    //   'vSolicitorName': null,
    //   'vSolicitorFirm': null,
    //   'vendorName': null,
    //   'vendorAddress': null,
    //   'vendorIdno': null,
    //   'purchaserName': null,
    //   'purchaserAddress': null,
    //   'purchaserIdno': null,
    //   'terms': null,
    //   'purchaserEndorse': false,
    //   'vendorEndorse': false,
    //   'pSolicitorEndorse': false,
    //   'vSolicitorEndorse': false,
    //   'endorse': 0,
    //   'active': true,
    //   'owner': null
    // });
    return this.serviceNewContract.addAsset(this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.successMessage = 'Successfully Added New Contract';
      this.myForm.setValue({
        'caseid': null,
        'property': null,
        'amount': null,
        'contractState': 'Draft',
        'pSolicitorName': null,
        'pSolicitorFirm': null,
        'vSolicitorName': null,
        'vSolicitorFirm': null,
        'vendorName': null,
        'vendorAddress': null,
        'vendorIdno': null,
        'purchaserName': null,
        'purchaserAddress': null,
        'purchaserIdno': null,
        'terms': null,
        'purchaserEndorse': false,
        'vendorEndorse': false,
        'pSolicitorEndorse': false,
        'vSolicitorEndorse': false,
        'endorse': 0,
        'active': true,
        'owner': null
      });
      //this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
          this.successMessage = null;
      } else {
          //this.errorMessage = error;
          this.errorMessage = "Could not add new Contract. Check for any missing fields."
          this.successMessage = null;
      }
    });
  }

}
