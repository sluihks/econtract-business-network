import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs/Observable';
import { EContract } from '../org.example.namespacetest';
import 'rxjs/Rx';

// Can be injected into a constructor
@Injectable()
export class CreateNewContractService {

  private NAMESPACE = 'EContract';

  constructor(private dataService: DataService<EContract>) {
  };
  public addAsset(itemToAdd: any): Observable<EContract> {
    return this.dataService.add(this.NAMESPACE, itemToAdd);
  }
}
