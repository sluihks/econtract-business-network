import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace org.example.namespacetest{
   export class EContract extends Asset {
      caseid: string;
      property: string;
      amount: string;
      contractState: State;
      pSolicitorName: string;
      pSolicitorFirm: string;
      vSolicitorName: string;
      vSolicitorFirm: string;
      vendorName: string;
      vendorAddress: string;
      vendorIdno: string;
      purchaserName: string;
      purchaserAddress: string;
      purchaserIdno: string;
      terms: string;
      purchaserEndorse: boolean;
      vendorEndorse: boolean;
      pSolicitorEndorse: boolean;
      vSolicitorEndorse: boolean;
      endorse: number;
      active: boolean;
      owner: User;
   }
   export enum State {
      Draft,
      Review,
      Revise,
      Endorse,
      Close,
   }
   export enum UserType {
      Vendor,
      Purchaser,
      VendorSolicitor,
      PurchaserSolicitor,
   }
   export class User extends Participant {
      username: string;
      idNo: string;
   }
   export class ProcessEContract extends Transaction {
      contract: EContract;
      newowner: User;
   }
   export class EndorseEContract extends Transaction {
      uType: UserType;
      user: User;
      contract: EContract;
   }
   export class UpdateEContract extends Transaction {
      reviseTerms: string;
      contract: EContract;
   }
   export class ReviewEContract extends Transaction {
      contract: EContract;
   }
   export class EContractNotify extends Event {
      contract: EContract;
   }
// }
