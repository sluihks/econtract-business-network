import { Component, OnInit } from '@angular/core';
import { UploadDataService } from './upload-data.service';

@Component({
  selector: 'app-upload-data',
  templateUrl: './upload-data.component.html',
  styleUrls: ['./upload-data.component.css'],
  providers: [UploadDataService]
})
export class UploadDataComponent implements OnInit {

  private csvData;

  constructor() { }

  ngOnInit() {
  }
  convertData(){
		// var data = document.getElementById("inputData").value;
  //       var dataArray = CSVtoArray(data);        
  //       var count =20;
  //       var fullArray = new Array(count);
  //       var total_count = 0;
  //       var total = dataArray.length;
  //       for(i=0;i<count;i++){
  //           var rowArray = new Array(14);
  //           for(j=0;j<15;j++){
  //               rowArray[j] = dataArray[total_count];
  //               total_count++;
  //           }
  //           fullArray[i] = rowArray;
  //       }
  //       //console.log(fullArray);
		// var session_ID = Math.floor(Math.random() * Math.floor(9999));
  //       for(i=1;i<fullArray.length;i++){
  //           var userID = session_ID + '-' + Math.floor(Math.random() * Math.floor(9999));
  //           var jsonUserAccount = [{$class: "org.example.namespacetest.User", username: "testuserJSON1", idNo: userID}];
  //           var jsonContractData = 

  //       }
    }
    // function sendRequest(){
    //     console.log("starting request");
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('GET', 'localhost:3000/api/User');
    //     //xhr.setRequestHeader('Content-Type', 'application/json');
    //     xhr.onload = function() {
    //         if (xhr.status === 200) {
    //             var userInfo = JSON.parse(xhr.responseText);
    //             console.log(userInfo);
    //         }else{
    //             alert('Request failed.  Returned status of ' + xhr.status);
    //         }
    //     };
    //     xhr.send();




        // xhr.send(JSON.stringify({
        //     //class: 'org.example.namespacetest.User',
        //     //username: 'testuserAjax1',
        //     idNo: '1'
        // }));
    //    console.log("ending request");
	//}
	CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

}
