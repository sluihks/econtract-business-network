/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs/Observable';
import { User } from '../org.example.namespacetest';
import { EContract } from '../org.example.namespacetest';
import 'rxjs/Rx';

// Can be injected into a constructor
@Injectable()
export class UploadDataService {

  private USER_NAMESPACE = 'User';
  private ECONTRACT_NAMESPACE = 'EContract';

  constructor(private dataService: DataService<any>) {
  };
  public addParticipant(itemToAdd: any): Observable<User> {
    return this.dataService.add(this.USER_NAMESPACE, itemToAdd);
  }
  public addAsset(itemToAdd: any): Observable<EContract> {
    return this.dataService.add(this.ECONTRACT_NAMESPACE, itemToAdd);
  }
}
