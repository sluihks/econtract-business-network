import { Component, OnInit } from '@angular/core';
import { UserSelectService } from './user-select.service';

@Component({
  selector: 'user-select',
  templateUrl: './user-select.component.html',
  styleUrls: ['./user-select.component.css'],
  providers: [UserSelectService]
})
export class UserSelectComponent implements OnInit {

	private allParticipants;
	private selectedValue;
	private errorMessage;
	private currentUser;
	private role: string;
  constructor(public serviceUser: UserSelectService) { 
  	this.currentUser = serviceUser.getCurrentUser;
  }

  ngOnInit() {
  	this.loadAll();
  	console.log(this.allParticipants);
  }
  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceUser.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(participant => {
        tempList.push(participant);
      });
      this.allParticipants = tempList;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        this.errorMessage = error;
      }
    });
  }
  public changeRole(){
  	console.log("Function changeRole");
  	console.log(this.selectedValue.username);
  	this.serviceUser.setCurrentUser(this.selectedValue.username);
  	console.log(this.serviceUser.getCurrentUser());
  	// this.serviceUser.role = this.selectedValue;
  }


}
