/**
 * To process asset Transfer
 * @param {org.example.namespacetest.ProcessEContract} trade
 * @transaction
 */
async function transferAsset(trade) {
  if (trade.contract.active) {
    // If contract is ready
    trade.contract.owner = trade.newowner;
    trade.contract.contractState = "Close";
    return getAssetRegistry("org.example.namespacetest.EContract")
      .then(assetRegistry => {
        return assetRegistry.update(trade.contract); // Update the network registry
      })
      .then(() => {
        let event = getFactory().newEvent(
          "org.example.namespacetest",
          "EContractNotify"
        ); // Get a reference to the event specified in the modeling language
        event.contract = trade.contract;
        emit(event); // Fire off the event
      });
  }
}




/**
 * To process asset Transfer
 * @param {org.example.namespacetest.EndorseEContract} endorse
 * @transaction
 */
async function endorseContract(endorse) {
  if (endorse.contract.active && endorse.contract.endorse < 4) {
    // Check who endorse
    switch(endorse.uType) {
 	 case "Vendor":
     if ( !endorse.contract.vendorEndorse) {  
  	  endorse.contract.vendorEndorse = true;
      endorse.contract.endorse++;
      endorse.contract.contractState = "Endorse";
     }
    break;
 	 case "Purchaser":
      if (!endorse.contract.purchaserEndorse && endorse.contract.endorse == 3) {
     endorse.contract.purchaserEndorse = true;
     endorse.contract.endorse++;endorse.contract.endorse < 4
     endorse.contract.owner = endorse.user;
          }
    break;
     case "PurchaserSolicitor":
       if (!endorse.contract.pSolicitorEndorse) {   
     endorse.contract.pSolicitorEndorse = true;
     endorse.contract.endorse++;
     endorse.contract.contractState = "Endorse";
       }
    break;
    case "VendorSolicitor":
    if (!endorse.contract.vSolicitorEndorse) {   
     endorse.contract.vSolicitorEndorse = true;
     endorse.contract.endorse++;
     endorse.contract.contractState = "Endorse";
    }
    break;
    }
    if (endorse.contract.endorse == 4) {
      endorse.contract.active = false;
      endorse.contract.contractState = "Close";
    }
    return getAssetRegistry("org.example.namespacetest.EContract")
      .then(assetRegistry => {
        return assetRegistry.update(endorse.contract); // Update the network registry
      })
      .then(() => {
        let event = getFactory().newEvent(
          "org.example.namespacetest",
          "EContractNotify"
        ); // Get a reference to the event specified in the modeling language
        event.contract = endorse.contract;
        emit(event); // Fire off the event
      });
  }
}

/**
 * To process update contract terms
 * @param {org.example.namespacetest.UpdateEContract} update
 * @transaction
 */
async function updateContract(update) {
  if (update.contract.active) {
    // If contract is under draft/review status
    update.contract.terms = update.reviseTerms;
    update.contract.contractState = "Revise";
    return getAssetRegistry("org.example.namespacetest.EContract")
      .then(assetRegistry => {
        return assetRegistry.update(update.contract); // Update the network registry
      })
      .then(() => {
        let event = getFactory().newEvent(
          "org.example.namespacetest",
          "EContractNotify"
        ); // Get a reference to the event specified in the modeling language
        event.contract = update.contract;
        emit(event); // Fire off the event
      });
  }
}


/**
 * To proceed review contract
 * @param {org.example.namespacetest.ReviewEContract} review
 * @transaction
 */
async function reviewContract(review) {
  if (review.contract.active) {
    // If contract is under draft/review status
    review.contract.contractState = "Review";
    return getAssetRegistry("org.example.namespacetest.EContract")
      .then(assetRegistry => {
        return assetRegistry.update(review.contract); // Update the network registry
      })
      .then(() => {
        let event = getFactory().newEvent(
          "org.example.namespacetest",
          "EContractNotify"
        ); // Get a reference to the event specified in the modeling language
        event.contract = review.contract;
        emit(event); // Fire off the event
      });
  }
}
